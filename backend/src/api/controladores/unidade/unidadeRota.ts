import { Router } from 'express';
import *  as unidadeController from './unidade.controller';

const routerUnidade = Router();
export const path = '/unidade';

routerUnidade.get(`${path}/buscaUnidades`, unidadeController.buscaUnidades)
routerUnidade.get(`${path}/buscaUnidade/:idUnidade`, unidadeController.buscaUnidade);
routerUnidade.post(`${path}/cadastraUnidade`, unidadeController.cadastraUnidade);
routerUnidade.get(`${path}/dashboard/valida/:email`, unidadeController.buscaUnidadePorEmail);
export { routerUnidade };