import { Request, Response, NextFunction } from 'express';
import { UnidadeSaude } from '../../../entidades/unidadeSaude';
import { UnidadeRepositorio } from '../../../persistencia/unidade/unidadeRepositorio';

export async function cadastraUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const unidade = req.body as UnidadeSaude;
        await UnidadeRepositorio.addUnidade(unidade);
        res.status(201).send("Cadastro Realizado");

    } catch (e) {
        next(e);
    }
}

export async function buscaUnidades(req: Request, res: Response, next: NextFunction) {
    try {
        const unidades = await UnidadeRepositorio.getUnidades();
        res.json(unidades);

    } catch (e) {
        next(e);
    }
}

export async function buscaUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const idUnidade = req.params.idUnidade;
        const unidade = await UnidadeRepositorio.getUnidadeById(idUnidade);
        res.json(unidade);

    } catch (e) {
        next(e);
    }
}

export async function buscaUnidadePorEmail(req: Request, res: Response, next: NextFunction){
    try {
        console.log(req.params.email);
        const email = req.params.email;
        const unidade = await UnidadeRepositorio.getUnidadeByEmail(email);
        res.json(unidade);
    } catch (e) {
        next(e);
    }
}
