
import *  as atendimentoController from './atendimento.controller';
import { Router } from 'express';

const routerPesoa = Router();
export const path = '/atendimento';

routerPesoa.get(`${path}/buscaatendimentos`, atendimentoController.buscaAtendimentos)
routerPesoa.post(`${path}/cadastraatendimento`, atendimentoController.cadastraAtendimento);
routerPesoa.patch(`${path}/alteraatendimento`, atendimentoController.alteraAtendimento);
routerPesoa.get(`${path}/unidade/:idUnidade`, atendimentoController.atendimentosByUnidade);
routerPesoa.get(`${path}/casosTotalPorDia`, atendimentoController.buscaCasosTotalPorDia);
routerPesoa.get(`${path}/casosNovosPorDia`, atendimentoController.buscaCasosNovosPorDia);
routerPesoa.post(`${path}/getNovosPorDiaPorUnidade`, atendimentoController.buscaCasosNovosPorDiaPorUnidade);
routerPesoa.get(`${path}/casosPorBairro`, atendimentoController.buscarCasosPorBairro);
routerPesoa.get(`${path}/tempoUnidade/:idUnidade`, atendimentoController.tempoUnidade);
routerPesoa.delete(`${path}/deletaAtendimento`, atendimentoController.deletarAtendimento);

export { routerPesoa };