import { Request, Response, NextFunction } from 'express';
import { Atendimento } from '../../../entidades/atendimento';
import { AtendimentoRepositorio } from '../../../persistencia/Atendimento/atendimentoRepositorio';

export async function cadastraAtendimento(req: Request, res: Response, next: NextFunction) {
    try {
        console.log(req.body)
        const atendimento = req.body as Atendimento;
        await AtendimentoRepositorio.cadastrarAtendimento(atendimento);
        res.status(201).send('Cadastro realizado');
    } catch (e) {
        next(e);
    }
}

export async function buscaAtendimentos(req: Request, res: Response, next: NextFunction) {
    try {
        const atendimentos = await AtendimentoRepositorio.getAtendimentos();
        res.json(atendimentos);
    } catch (e) {
        next(e);
    }
}
export async function deletarAtendimento(req: Request, res: Response, next: NextFunction) {
    try {
        const a = req.body.id;
        const atendimentos = await AtendimentoRepositorio.deletarAtendimento(a);
        res.json(atendimentos);
    } catch (e) {
        next(e);
    }}
export async function buscaCasosTotalPorDia(req: Request, res: Response, next: NextFunction){
    try {
        const atendimentos = await AtendimentoRepositorio.getAtendimentosTotalPorDia();
        res.json(atendimentos);
    } catch (e) {
        next(e);
    }
}
export async function buscaCasosNovosPorDia(req: Request, res: Response, next: NextFunction){
    try {
        const atendimentos = await AtendimentoRepositorio.getCasosNovosPorDia();
        res.json(atendimentos);
    } catch (e) {
        next(e);
    }
}
export async function buscaCasosNovosPorDiaPorUnidade(req: Request, res: Response, next: NextFunction){
    try {
        const unidade = req.body.unidade;
        const atendimentos = await AtendimentoRepositorio.getCasosPorNovosPorDiaPorUnidade(unidade);
        res.status(200).json(atendimentos);
    } catch (e) {
        next(e);
    }
};
export async function buscarCasosPorBairro(req: Request, res: Response, next: NextFunction){
    try {
        const atendimentos = await AtendimentoRepositorio.getCasosPorBairro();
        res.json(atendimentos);
    } catch (e) {
        next(e);
    }
}


export async function buscaAtendimento(req: Request, res: Response, next: NextFunction) {
    try {
        const idAtendimento = req.params.idAtendimento;
        const atendimento = await AtendimentoRepositorio.getAtendimentoById(idAtendimento);
        res.json(atendimento);

    } catch (e) {
        next(e);
    }
}
export async function alteraAtendimento(req: Request, res: Response, next: NextFunction) {
    try {
        const atendimento = req.body as Atendimento;

        await AtendimentoRepositorio.editarAtendimento(atendimento.idAtendimento, atendimento);
        res.status(204).send('Cadastro alterado');

    } catch (e) {

    }
}

export async function atendimentosByUnidade(req: Request, res: Response, next: NextFunction) {
    try {
        const idUnidade = req.params.idUnidade;
        const Atendimentos = await AtendimentoRepositorio.getAtendimentosPorUnidade(idUnidade);
        res.status(200).json(Atendimentos);
    } catch (e) {

    }


}
export async function tempoUnidade(req: Request, res: Response, next: NextFunction){
    try {
        const idUnidade = req.params.idUnidade;
        const tempo = await AtendimentoRepositorio.getTemposUnidades(idUnidade);
        res.json(tempo);
    } catch (e) {
        
    }
}