import app from './app';
import { connect } from 'mongoose';
import {AtendimentoRepositorio} from './persistencia/Atendimento/atendimentoRepositorio';
import { UnidadeRepositorio } from './persistencia/unidade/unidadeRepositorio';
(async () => {
    try {
        const dbAtlas = 'mongodb+srv://db:db@cluster0.bpclc.mongodb.net/pandemia?retryWrites=true&w=majority';
        await connect(dbAtlas, { useNewUrlParser: true });
        app.listen(8080, () => {
            console.log('on: 8080');
        });
            } catch (e) {
        console.log(e);
        console.log(e.message);
    }
})();
