const request = require('supertest')
import app from '../app';
jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

describe('Unidade controller', () => {
    describe('Busca unidades', () => {
        test('deve retornr status 200', async () => {
            const res = await request("http://localhost:8080")
                .get('/unidade/buscaunidades').send()

            expect(res.statusCode).toEqual(200)
            expect(res.body).toBeDefined();
        });
    });

    describe('Unidade controller', () => {
        describe('Busca unidade', () => {
            test('deve retornr status 200', async () => {
                const res = await request("http://localhost:8080")
                    .get('/unidade/buscaunidade/1').send()

                expect(res.statusCode).toEqual(200)
                expect(res.body).toBeDefined();
            });
        });
    });
    describe('Unidade controller', () => {
        describe('Busca unidade', () => {
            test('deve retornr status 200', async () => {
                const res = await request("http://localhost:8080")
                    .get('/unidade/dashboard/valida/test@email.com').send()

                expect(res.statusCode).toEqual(200)
                expect(res.body).toBeDefined();
            });
        });
    });
});