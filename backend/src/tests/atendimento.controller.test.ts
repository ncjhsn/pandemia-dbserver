const request = require('supertest')
import app from '../app';
jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

describe('Atendimento controller', () => {
  describe('Busca Atendimentos', () => {
    test('deve retornr status 200', async () => {
      const res = await request("http://localhost:8080")
        .get('/atendimento/buscaatendimentos').send()

      expect(res.statusCode).toEqual(200)
      expect(res.body).toBeDefined();
    });
  });

  /*describe('Add atendimento', () => {
    test('Deve retornar status 201', async () => {
    const res = await request("http://localhost:8080")
      .post('/atendimento/cadastraatendimento').send({
        idAtendimento: '0',
        nome: 'asdas',
        date: '2020-08-19T00:00:00.000Z',
        'endereço': { rua: 'sadas', bairro: 'asdas' },
        laudo: false,
        test1: false,
        test2: true,
        duracao: 22,
        unidadeSaude: {
          idUnidade: '8',
          nome: 'AAAAAA',
          email: 'test@email.com',
          endereco: 'dsadasd',
          capacidade: 2
        }
      })
      
    expect(res.statusCode).toEqual(201);
  });
});*/

  describe('edita Atendimento', () => {
    test('Deve retornar status 204', async () => {
      const res = await request("http://localhost:8080")
        .patch('/atendimento/alteraatendimento').send({
          "idAtendimento": '1',
          "nome": 'João P.',
          "date": "2020-08-12T00:00:00.000Z",
          "endereço": {
              "rua": "Garibaldi ",
              "bairro": "Anchieta "
          },
          "laudo": true,
          "test1": true,
          "test2": true,
          "duracao": 20,
          "unidadeSaude": {
              "idUnidade": "1",
              "nome": "Unidade Santo Antonio ",
              "email": "jose@email.com",
              "endereco": " Alameda Vinte de Agosto",
              "capacidade": 30
          }
        })

      expect(res.statusCode).toEqual(204);
    });
  });

  describe('Busca Atendimentos por unidadeSaude', () => {
    test('deve retornr status 200', async () => {
      const res = await request("http://localhost:8080")
        .get('/atendimento/unidade/1').send()

      expect(res.statusCode).toEqual(200)
      expect(res.body).toBeDefined();
    });

    describe('Busca casosTotalPorDia', () => {
      test('deve retornr status 200', async () => {
        const res = await request("http://localhost:8080")
          .get('/atendimento/casosTotalPorDia').send()

        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeDefined();
      });
    });
    describe('Busca casosTotalPorDia', () => {
      test('deve retornr status 200', async () => {
        const res = await request("http://localhost:8080")
          .get('/atendimento/casosTotalPorDia').send()

        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeDefined();
      });
    });

    describe('Busca casosNovosPorDia', () => {
      test('deve retornr status 200', async () => {
        const res = await request("http://localhost:8080")
          .get('/atendimento/casosNovosPorDia').send()

        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeDefined();
      });
    });
    describe('Busca getNovosPorDiaPorUnidade', () => {
      test('deve retornr status 200', async () => {
        const res = await request("http://localhost:8080")
          .post('/atendimento/getNovosPorDiaPorUnidade').send({ unidade: "1" })

        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeDefined();
      });
    });
    describe('Busca casos por bairro', () => {
      test('deve retornr status 200', async () => {
        const res = await request("http://localhost:8080")
          .get('/atendimento/casosPorBairro').send()

        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeDefined();
      });
    });
    describe('Busca tempo das unidades', () => {
      test('deve retornr status 200', async () => {
        const res = await request("http://localhost:8080")
          .get('/atendimento/tempoUnidade').send({ idUnidade: "1" })

        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeDefined();
      });
    });

  });
});
