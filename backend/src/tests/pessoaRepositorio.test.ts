import mongoose from 'mongoose';
import * as dbhandler from './dbhandler';
import { AtendimentoRepositorio } from '../persistencia/Atendimento/atendimentoRepositorio';
import { AtendimentoModel } from '../persistencia/Atendimento/atendimentoModel';
import { Atendimento } from '../entidades/atendimento'
import console from 'console';
jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;



beforeAll(async () => {
    await dbhandler.connect();
});
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Atendimento repositorio ', () => {
    const AtendimentoMock: Atendimento = {
        idAtendimento: "0",
        date: new Date('2020-09-20'),
        nome: 'Silvio Andrades',
        laudo: true,
        test1: true,
        test2: true,
        duracao: 5,
        endereço: {
            bairro: 'bairro A',
            rua: 'Rua A'
        },
        unidadeSaude: {
            idUnidade: '1',
            nome: 'A',
            email: 'test@email.com',
            endereco: 'AAA',
            capacidade: 5
        }
    };
    const AtendimentoMockParaEdição: Atendimento = {
        idAtendimento: '0',
        date: new Date('2020-09-20'),
        nome: 'Silvio editado',
        laudo: true,
        test1: true,
        test2: true,
        duracao: 5,
        endereço: {
            bairro: 'bairro A',
            rua: 'Rua A'
        },
        unidadeSaude: {
            idUnidade: '1',
            nome: 'A',
            email: 'test@email.com',
            endereco: 'AAA',
            capacidade: 5
        }
    }

    const AtendimentoMockSemNome: Atendimento = {
        idAtendimento: "1",
        date: new Date('2020-09-20'),
        nome: '',
        laudo: true,
        test1: true,
        test2: true,
        duracao: 5,
        endereço: {
            bairro: 'bairro A',
            rua: 'Rua A'
        }, unidadeSaude: {
            idUnidade: '1',
            nome: 'A',
            email: 'test@email.com',
            endereco: 'AAA',
            capacidade: 5
        }
    };
    const AtendimentoMockSemTest: Atendimento = {
        idAtendimento: "2",
        date: new Date('2020-09-20'),
        nome: 'Paulo',
        laudo: true,
        duracao: 5,
        endereço: {
            bairro: 'bairro A',
            rua: 'Rua A'
        }, unidadeSaude: {
            idUnidade: '1',
            nome: 'A',
            email: 'test@email.com',
            endereco: 'AAA',
            capacidade: 5
        }
    };
    const AtendimentoMockSemUnidadeDeSaude: Atendimento = {
        idAtendimento: "3",
        date: new Date('2020-09-20'),
        nome: 'Silvio Andrades',
        laudo: true,
        test1: true,
        test2: true,
        duracao: 5,
        endereço: {
            bairro: 'bairro A',
            rua: 'Rua A'
        }, unidadeSaude: {
            idUnidade: '',
            email: 'test@email.com',
            nome: '',
            endereco: '',
            capacidade: NaN
        }
    };
    describe("cadastrarAtendimento() ", () => {
        test('O retorno do  cadastro deve ser igual ao obj enviado', async () => {
            const res = await AtendimentoRepositorio.cadastrarAtendimento(AtendimentoMock);
            expect(res.idAtendimento).toEqual(AtendimentoMock.idAtendimento);
            expect(res.nome).toEqual(AtendimentoMock.nome);
            expect(res.laudo).toEqual(AtendimentoMock.laudo);
            expect(res.unidadeSaude).toEqual(AtendimentoMock.unidadeSaude);
        });
        test("Deve cadastrar uma Atendimento sem error", async () => {
            expect(async () => {
                await AtendimentoRepositorio.cadastrarAtendimento(AtendimentoMock);
            }).not.toThrow();
        });
        test("nome tem que ser valido", async () => {
            expect(async () => {
                await AtendimentoRepositorio.cadastrarAtendimento(AtendimentoMockSemNome);
            }).rejects.toThrow(mongoose.Error.ValidationError);
        });
    });
    describe("editar()", () => {
        test("A edição tem que retornar o valor de n=1 para id valido", async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            const resp = await AtendimentoRepositorio.editarAtendimento("1", AtendimentoMockParaEdição);
            expect(resp.nModified).toEqual(1);
        });
        test('A edição tem que gerar erro se o id for invalido', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            expect(async () => {
                const resp = await AtendimentoRepositorio.editarAtendimento("4", AtendimentoMockParaEdição);
            }).rejects.toThrowError();
        });
    });
    describe('deletarAtendimento()', () => {
        test('deletar Atendimento valida deve retornar n=1', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            const result = await AtendimentoRepositorio.deletarAtendimento('1');
            expect(result.n).toEqual(1);
        });
        test('Deletar Atendimento invalida deve gerar um error ', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            expect(async () => {
                const result = await AtendimentoRepositorio.deletarAtendimento('invalido');
            }).rejects.toThrowError();
        })
    });
    describe('getAtendimentos() ', () => {
        test('O retorno deve ser de todos as Atendimentos do addToDataBase', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            const resp = await AtendimentoRepositorio.getAtendimentos();
            expect(resp).toBeDefined();
            expect(resp).toHaveLength(3);
            expect(resp[0].nome).toEqual('Fulano 1');
            expect(resp[1].nome).toEqual('Fulano 2');
            expect(resp[2].nome).toEqual('Fulano 3');

        });
        test('Se nao tiver nada no bd deve retornar um array vazio', async () => {
            await dbhandler.clearDatabase();
            const resp = await AtendimentoRepositorio.getAtendimentos();
            expect(resp).toBeDefined();
            expect(resp).toHaveLength(0);
        });
    });
    describe('getAtendimentoByid() ', () => {
        test('getAtendimentoByid, caso um id existente, deve retornar o obj solicitado. ', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            const resp = await AtendimentoRepositorio.getAtendimentoById('1');
            expect(resp.nome).toEqual('Fulano 1');
        });
        test('getAtendimentoByid, caso id inexistente deve gerar um erro', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            expect(async () => {
                const resp = await AtendimentoRepositorio.getAtendimentoById('4');
            }).rejects.toThrowError();
        });
    });
    describe('getAtendimentosPorUnidade()', () => {
        test('Deve retornar a quantidade certa de Atendimentos com aquela unidade', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            const result = await AtendimentoRepositorio.getAtendimentosPorUnidade('1');
            const result2 = await AtendimentoRepositorio.getAtendimentosPorUnidade('11');
            expect(result).toBeDefined();
            expect(result).toHaveLength(2);
            expect(result[0].nome).toEqual('Fulano 1');
            expect(result2).toBeDefined();
            expect(result2).toHaveLength(1);
            expect(result2[0].nome).toEqual('Fulano 2');
            expect(result[1].nome).toEqual('Fulano 3');
        });
        test('Deve gerar um erro caso nao exista Atendimento com unidade solicitada', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            expect(async () => {
                const result = await AtendimentoRepositorio.getAtendimentosPorUnidade('222');
            }).rejects.toThrowError();
        });

    });

    describe('getAtendimentoTotalPorDia()', () => {
        test('deve retornar o valor certo para os mocks', async () => {
            await dbhandler.clearDatabase();
            await addToDataBaseParaOsGraficos();
            const resp = await AtendimentoRepositorio.getAtendimentosTotalPorDia();
            expect(resp).toBeDefined();
            expect(resp).toHaveLength(2);
            expect(resp[0].casos).toEqual(1);
            expect(resp[0].dia).toEqual(new Date('2020-09-20').toDateString());

        });
    });
    describe('getCasosNovosPorDia()', () => {
        test('deve retornar o valor certo para os mocks', async () => {
            await dbhandler.clearDatabase();
            await addToDataBaseParaOsGraficos();
            const resp = await AtendimentoRepositorio.getCasosNovosPorDia();
            expect(resp).toBeDefined();
            expect(resp).toHaveLength(2);
            expect(resp[0].casos).toEqual(1);
            expect(resp[0].dia).toEqual(new Date('2020-09-20').toDateString());

        });
    });
    describe('getCasosPorBairro()', () => {
        test('deve retornar o valor certo para os mocks', async () => {
            await dbhandler.clearDatabase();
            await addToDataBaseParaOsGraficos();
            const resp = await AtendimentoRepositorio.getCasosPorBairro();
            expect(resp).toBeDefined();
            expect(resp).toHaveLength(2);
            expect(resp[0].bairro).toEqual('bairro A');
            expect(resp[0].casos).toEqual(1);

        });
    });
    describe('getTemposUnidades()', () => {
        test('deve retornar o valor certo para os mocks', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            const resp = await AtendimentoRepositorio.getTemposUnidades('1');
            expect(resp.medio).toEqual(7.5);
            expect(resp.maximo).toEqual(10);
            expect(resp.minimo).toEqual(5);

        });
    });
    describe('getCasosPorNovosPorDiaPorUnidade() ', () =>{
        test('getCasosPorNovosPorDia deve retornar o valor certo para os mocks', async ()=>{
            await dbhandler.clearDatabase();
            await addToDataBaseParaOsGraficos();
            const resp = await AtendimentoRepositorio.getCasosPorNovosPorDiaPorUnidade('1');
            expect(resp[0].dia).toEqual(new Date('2020-09-20').toDateString());
            expect(resp[0].casos).toEqual(1);
        });
    });
    describe('getAtendimentosTotalPorDia() ', () =>{
        test('getAtendimentosTotalPorDia deve retornar o valor certo para os mocks', async ()=>{
            await dbhandler.clearDatabase();
            await addToDataBaseParaOsGraficos();
            const resp = await AtendimentoRepositorio.getAtendimentosTotalPorDia();
            expect(resp[0].dia).toEqual(new Date('2020-09-20').toDateString());
            expect(resp[0].casos).toEqual(1);
        });
    });
    describe('getCasosNovosPorDia() ', () =>{
        test('getCasosNovosPorDia deve retornar o valor certo para os mocks', async ()=>{
            await dbhandler.clearDatabase();
            await addToDataBaseParaOsGraficos();
            const resp = await AtendimentoRepositorio.getCasosNovosPorDia();
            expect(resp[0].dia).toEqual(new Date('2020-09-20').toDateString());
            expect(resp[0].casos).toEqual(1);
        });
    });
    describe('getCasosPorBairro() ', () =>{
        test('getCasosPorBairro deve retornar o valor certo para os mocks', async ()=>{
            await dbhandler.clearDatabase();
            await addToDataBaseParaOsGraficos();
            const resp = await AtendimentoRepositorio.getCasosPorBairro();
            expect(resp[0].bairro).toEqual('bairro A');
            expect(resp[0].casos).toEqual(1);
        });
    });

    async function addToDataBase() {
        let Atendimento1: Atendimento = {
            idAtendimento: "0",
            date: new Date('2020-09-20'),
            nome: 'Fulano 1',
            laudo: true,
            test1: true,
            test2: true,
            duracao: 5,
            endereço: {
                bairro: 'bairro A',
                rua: 'Rua A'
            }, unidadeSaude: {
                idUnidade: '1',
                nome: 'A',
                endereco: 'AAA',
                capacidade: 5,
                email: 'test@email.com',

            }
        };
        let Atendimento2: Atendimento = {
            idAtendimento: "1",
            date: new Date('2020-09-20'),
            nome: 'Fulano 2',
            laudo: false,
            test1: false,
            test2: false,
            duracao: 5,
            endereço: {
                bairro: 'bairro A',
                rua: 'Rua A'
            }, unidadeSaude: {
                idUnidade: '11',
                nome: 'aA',
                endereco: 'AbAA',
                capacidade: 5,
                email: 'test@email.com',

            }
        };
        let Atendimento3: Atendimento = {
            idAtendimento: "2",
            date: new Date('2020-09-21'),
            nome: 'Fulano 3',
            laudo: true,
            test1: true,
            test2: true,
            duracao: 10,
            endereço: {
                bairro: 'bairro B',
                rua: 'Rua A'
            }, unidadeSaude: {
                idUnidade: '1',
                nome: 'aA',
                email: 'test@email.com',
                endereco: 'AbAA',
                capacidade: 15,
            }
        };
        await AtendimentoRepositorio.cadastrarAtendimento(Atendimento1);
        await AtendimentoRepositorio.cadastrarAtendimento(Atendimento2);
        await AtendimentoRepositorio.cadastrarAtendimento(Atendimento3);
    }

    async function addToDataBaseParaOsGraficos() {
        let Atendimento1: Atendimento = {
            idAtendimento: "0",
            date: new Date('2020-09-20'),
            nome: 'Fulano 1',
            laudo: true,
            test1: true,
            test2: true,
            duracao: 5,
            endereço: {
                bairro: 'bairro A',
                rua: 'Rua A'
            }, unidadeSaude: {
                idUnidade: '1',
                nome: 'A',
                endereco: 'AAA',
                capacidade: 5,
                email: 'test@email.com',

            }
        };
        let Atendimento2: Atendimento = {
            idAtendimento: "1",
            date: new Date('2020-09-20'),
            nome: 'Fulano 2',
            laudo: false,
            test1: false,
            test2: false,
            duracao: 5,
            endereço: {
                bairro: 'bairro A',
                rua: 'Rua A'
            }, unidadeSaude: {
                idUnidade: '11',
                nome: 'aA',
                endereco: 'AbAA',
                capacidade: 5,
                email: 'test@email.com',

            }
        };
        let Atendimento3: Atendimento = {
            idAtendimento: "2",
            date: new Date('2020-09-21'),
            nome: 'Fulano 3',
            laudo: true,
            test1: true,
            test2: true,
            duracao: 10,
            endereço: {
                bairro: 'bairro B',
                rua: 'Rua A'
            }, unidadeSaude: {
                idUnidade: '1',
                nome: 'aA',
                email: 'test@email.com',
                endereco: 'AbAA',
                capacidade: 15,
            }
        };
        let Atendimento4: Atendimento = {
            idAtendimento: "2",
            date: new Date('2020-09-21'),
            nome: 'Fulano 3',
            laudo: true,
            test1: true,
            test2: true,
            duracao: 10,
            endereço: {
                bairro: 'bairro B',
                rua: 'Rua A'
            }, unidadeSaude: {
                idUnidade: '1',
                nome: 'aA',
                email: 'test@email.com',
                endereco: 'AbAA',
                capacidade: 15,
            }
        };
        await AtendimentoRepositorio.cadastrarAtendimento(Atendimento1);
        await AtendimentoRepositorio.cadastrarAtendimento(Atendimento2);
        await AtendimentoRepositorio.cadastrarAtendimento(Atendimento3);
        await AtendimentoRepositorio.cadastrarAtendimento(Atendimento4);

    }


});
