import mongoose from 'mongoose';
import * as dbhandler from './dbhandler';
import { UnidadeRepositorio } from '../persistencia/unidade/unidadeRepositorio';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

beforeAll(async () => {
    await dbhandler.connect();
});
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('Unidade Repositorio', () => {
    const unidadeMock = {
        idUnidade: '0',
        nome: 'Unidade de saude 1',
        endereco: 'Rua lalala',
        email: 'test@email.com',
        capacidade: 20
    }

    const unidadeMockSemNome = {
        idUnidade: '0',
        nome: '',
        endereco: 'Rua lalala',
        email: 'test@email.com',
        capacidade: 20
    }
    const unidadeMockSemEndereco = {
        idUnidade: '0',
        nome: 'Unidade de saude 1',
        endereco: '',
        email: 'test2@email.com',
        capacidade: 20
    }
    const unidadeMockSemCapacidade = {
        idUnidade: '0',
        nome: 'Unidade de saude 1',
        endereco: 'Rua lalala',
        email: 'test3@email.com',
        capacidade: NaN
    }

    describe('addUnidade()', () => {
        test('deve add sem error um objeto valido', async () => {
            const res = await UnidadeRepositorio.addUnidade(unidadeMock);
            expect(res.idUnidade).toEqual(unidadeMock.idUnidade);
            expect(res.nome).toEqual(unidadeMock.nome);
            expect(res.endereco).toEqual(unidadeMock.endereco);
            expect(res.capacidade).toEqual(unidadeMock.capacidade);
        });
        test("Deve gerar error se add obj sem nome", async () => {
            expect(async () => {
                const res = await UnidadeRepositorio.addUnidade(unidadeMockSemNome);
            }).rejects.toThrowError(mongoose.Error.ValidationError);
        });
        test("Deve gerar error se add obj sem endereço", async () => {
            expect(async () => {
                const res = await UnidadeRepositorio.addUnidade(unidadeMockSemEndereco);
            }).rejects.toThrowError(mongoose.Error.ValidationError);
        });
        test("Deve gerar error se add obj sem capacidade", async () => {
            expect(async () => {
                const res = await UnidadeRepositorio.addUnidade(unidadeMockSemCapacidade);
            }).rejects.toThrowError(mongoose.Error.ValidationError);
        });
    });
    describe('getUnidade', () => {
        test('getUnidades Deve retornar uma lista com as unidades do addToDataBase', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            const res = await UnidadeRepositorio.getUnidades();
            expect(res).toBeDefined();
            expect(res).toHaveLength(3);
            expect(res[0].idUnidade).toEqual('1');
            expect(res[1].idUnidade).toEqual('2');
            expect(res[2].idUnidade).toEqual('3');
        });
        test('getUnidades deve retornar uma lista vazia se nao tiver nada no banco', async () => {
            await dbhandler.clearDatabase();
            const res = await UnidadeRepositorio.getUnidades();
            expect(res).toBeDefined();
            expect(res).toHaveLength(0);
        });
    });
    describe('getUnidadePorId()', () => {
        test('deve retornar o obj solicitado se tiver id valido', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            const res = await UnidadeRepositorio.getUnidadeById('1');
            expect(res.idUnidade).toEqual('1');
            expect(res.nome).toEqual('Unidade de saude 1');
            expect(res.endereco).toEqual('Rua lalala');
            expect(res.capacidade).toEqual(20);
        });
        test('Deve gerar error se o id for invalido', async () => {
            await dbhandler.clearDatabase();
            await addToDataBase();
            expect(async () => {
                const res = await UnidadeRepositorio.getUnidadeById('111');
            }).rejects.toThrowError();
        });
        describe('GetUnidadePorEmail ()',() =>{
            test('Deve retornar a unidade por email', async () => {
                await dbhandler.clearDatabase();
                await addToDataBase();
                const res = await UnidadeRepositorio.getUnidadeByEmail('test@email.com');
                expect(res?.endereco).toEqual('Rua lalala');
            });
        });
    });

    async function addToDataBase() {
        const unidadeMock1 = {
            idUnidade: '0',
            nome: 'Unidade de saude 1',
            endereco: 'Rua lalala',
            email: 'test@email.com',
            capacidade: 20
        }
        const unidadeMock2 = {
            idUnidade: '1',
            nome: 'Unidade de saude 2',
            endereco: 'Rua lelele',
            email: 'test2@email.com',
            capacidade: 10
        }
        const unidadeMock3 = {
            idUnidade: '2',
            nome: 'Unidade de saude 3',
            email: 'test3@email.com',
            endereco: 'Rua lilili',
            capacidade: 30
        }
        await UnidadeRepositorio.addUnidade(unidadeMock1);
        await UnidadeRepositorio.addUnidade(unidadeMock2);
        await UnidadeRepositorio.addUnidade(unidadeMock3);
    };
});
