import { Model, model, Schema, Document } from "mongoose";
import { UnidadeSaude } from "../../entidades/unidadeSaude";

interface UnidadeDocument extends UnidadeSaude, Document { }

export const UnidadeModel: Model<UnidadeDocument> = model<UnidadeDocument>('unidade', new Schema({
    idUnidade: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    endereco: {
        type: String,
        required: true
    },
    capacidade: {
        type: Number,
        required: true
    }
}), 'unidades');