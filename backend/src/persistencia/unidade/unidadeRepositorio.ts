import { UnidadeSaude } from "../../entidades/unidadeSaude";
import { UnidadeModel } from "../unidade/unidadeModel";

export class UnidadeRepositorio {
    static async addUnidade(unidade: UnidadeSaude): Promise<UnidadeSaude> {
        unidade.idUnidade = (await UnidadeModel.countDocuments() + 1).toString();
        let res = await UnidadeModel.create(unidade);
        return res.save();
    }

    static async getUnidades(): Promise<UnidadeSaude[]> {
        let unidades = await UnidadeModel.find().exec();
        return unidades;
    }

    static async getUnidadeById(id: string): Promise<UnidadeSaude | any> {
        let unidade = await UnidadeModel.findOne().where("idUnidade").equals(id).exec();
        if(unidade == null){
            throw new Error('id inexistente');
        }
        return unidade;
    }
    static async getUnidadeByEmail(e: string): Promise<UnidadeSaude | null>{
        let unidade = await UnidadeModel.findOne({email: e}).exec();
        console.log(await UnidadeModel.countDocuments());
        return unidade;
    }
}