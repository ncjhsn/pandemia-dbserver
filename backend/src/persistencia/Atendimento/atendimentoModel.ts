import { Atendimento } from "../../entidades/atendimento";
import {UnidadeSaude} from "../../entidades/unidadeSaude";
import { Model, model, Schema, Document } from "mongoose";
import { UnidadeModel } from "../unidade/unidadeModel";

interface AtendimentoDocument extends Atendimento, Document { }

export const AtendimentoModel: Model<AtendimentoDocument> = model<AtendimentoDocument>('Atendimento', new Schema({
    idAtendimento: {
        type: String,
        required: true
    },
    date:{
        type: Date,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    endereço: {
        type: {
            rua: {
                type: String,
                required: true
            },
            bairro:{
                type: String,
                required: true
            }
        },
        required: true
    },
    laudo: {
        type: Boolean,
        required: true
    },
    test1: {
        type: Boolean,
    },
    test2: {
        type: Boolean,
    },  
    duracao: {
        type: Number,
        required: true
    },
    unidadeSaude: {
        type: {
            idUnidade: {
                type: String,
                required: true
            },
            nome: {
                type: String,
                required: true
            },
            endereco: {
                type: String,
                required: true
            },
            capacidade: {
                type: Number,
                required: true
            }},
        required: true
    }
}), 'Atendimentos');