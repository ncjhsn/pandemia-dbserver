import { Atendimento } from "../../entidades/atendimento";
import { AtendimentoModel } from './atendimentoModel';
import { UnidadeSaude } from "../../entidades/unidadeSaude";

export class AtendimentoRepositorio {
    static async cadastrarAtendimento(Atendimento: Atendimento): Promise<Atendimento> {
        Atendimento.idAtendimento = (await AtendimentoModel.countDocuments() + 1).toString();
        if (Atendimento.test1 || Atendimento.test2) {
            Atendimento.laudo = true;
        }
        let cadAtendimento = await AtendimentoModel.create(Atendimento);
        return cadAtendimento.save();
    }

    static async editarAtendimento(id: string, Atendimento: Atendimento): Promise<any> {
        let pess = await AtendimentoModel.replaceOne({ idAtendimento: id }, Atendimento).exec();
        if (pess.n == 0) {
            throw new Error('id inexistente');
        }
        return pess;
    }

    static async deletarAtendimento(id: string): Promise<{
        ok?: number | undefined;
        n?: number | undefined;
    }> {
        let res = await AtendimentoModel.deleteMany({ idAtendimento: id }).exec();
        if (res.n == 0) {
            throw new Error('Atendimento inexistente');
        }
        return res;
    }

    static async getAtendimentos(): Promise<Atendimento[]> {
        let Atendimentos = await AtendimentoModel.find().exec();
        return Atendimentos;

    }

    static async getAtendimentoById(id: string): Promise<Atendimento | any> {
        let res = await AtendimentoModel.findOne().where('idAtendimento').equals(id).exec();
        if (res == null) {
            throw new Error('Id inexistente');
        }
        return res;
    }

    static async getAtendimentosPorUnidade(unidadeId: String): Promise<Atendimento[]> {
        let Atendimentos = await AtendimentoModel.find().where('unidadeSaude').exec();
        Atendimentos = Atendimentos.filter(pess => pess.unidadeSaude.idUnidade == unidadeId);
        if (Atendimentos.length == 0) {
            throw new Error('unidade inexistente');
        }
        return Atendimentos;
    }
    static async getCasosPorNovosPorDiaPorUnidade(unidade: string): Promise<{
        dia: string,
        casos: number
    }[]> {
        let pess = await AtendimentoModel.find({ laudo: true }).exec();
        let AtendimentosOrdanadasPorDia: Atendimento[] = [];
        let casosNovosPorDia: {
            dia: string,
            casos: number
        }[] = [];
        AtendimentosOrdanadasPorDia = pess.slice().sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
        AtendimentosOrdanadasPorDia.forEach((p) => {
            if (p.unidadeSaude.idUnidade == unidade) {
                let cont = 0;
                casosNovosPorDia.forEach(caso => {
                    if (caso.dia == new Date(p.date).toDateString()) {
                        caso.casos++;
                        cont++;
                    }
                });
                if (cont == 0) {
                    casosNovosPorDia.push({
                        dia: new Date(p.date).toDateString(),
                        casos: 1
                    });
                }
            }
        });
        return casosNovosPorDia;
    }


    static async getAtendimentosTotalPorDia(): Promise<{
        dia: string,
        casos: number
    }[]> {
        let AtendimentosOrdanadasPorDia: Atendimento[] = [];
        let arrayAtendimentosCasosPorDia: {
            dia: string,
            casos: number
        }[] = [];
        let pess = await AtendimentoModel.find({ laudo: true }).exec();
        let soma: number = 0;
        AtendimentosOrdanadasPorDia = pess.slice().sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
        AtendimentosOrdanadasPorDia.forEach((pesso) => {
            let cont = 0;
            soma++;
            arrayAtendimentosCasosPorDia.forEach(p => {
                if (new Date(pesso.date).toDateString() == p.dia) {
                    p.casos = soma;
                    cont++;
                }
            });
            if (cont == 0) {
                arrayAtendimentosCasosPorDia.push({
                    dia: new Date(pesso.date).toDateString(),
                    casos: soma
                });
            }
        });

        return arrayAtendimentosCasosPorDia;
    }

    static async getCasosNovosPorDia(): Promise<{
        dia: string,
        casos: number
    }[]> {
        let pess = await AtendimentoModel.find({ laudo: true }).exec();
        let AtendimentosOrdanadasPorDia: Atendimento[] = [];
        let casosNovosPorDia: {
            dia: string,
            casos: number
        }[] = [];
        AtendimentosOrdanadasPorDia = pess.slice().sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
        AtendimentosOrdanadasPorDia.forEach((p) => {
            let cont = 0;
            casosNovosPorDia.forEach(caso => {
                if (caso.dia == new Date(p.date).toDateString()) {
                    caso.casos++;
                    cont++;
                }
            });
            if (cont == 0) {
                casosNovosPorDia.push({
                    dia: new Date(p.date).toDateString(),
                    casos: 1
                });
            }
        });
        return casosNovosPorDia;
    }

    static async getCasosPorBairro(): Promise<{
        bairro: string,
        casos: number
    }[]> {
        let pess = await AtendimentoModel.find({ laudo: true }).exec();
        let AtendimentosOrdanadasPorDia: Atendimento[] = [];
        let casosPorBairro: {
            bairro: string,
            casos: number
        }[] = [];
        AtendimentosOrdanadasPorDia = pess.slice().sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
        AtendimentosOrdanadasPorDia.forEach(pess => {
            let cont = 0;
            casosPorBairro.forEach(caso => {
                if (caso.bairro == pess.endereço.bairro) {
                    caso.casos++;
                    cont++;
                }
            });
            if (cont == 0) {
                casosPorBairro.push({ bairro: pess.endereço.bairro, casos: 1 })
            }
        });
        return casosPorBairro;
    }

    static async getTemposUnidades(unidadeSaudeId: string): Promise<{
        maximo: number,
        medio: number,
        minimo: number
    }> {
        let max: number = 0;
        let med: number = 0;
        let min: number = 0;
        let cont: number = 0;
        let res = await AtendimentoModel.find().exec();
        min = res[0].duracao;
        res.forEach(r => {
            if (r.unidadeSaude.idUnidade == unidadeSaudeId) {
                cont++;
                if (r.duracao > max) {
                    max = r.duracao;
                }
                if (r.duracao < min) {
                    min = r.duracao;
                }
                med = med + r.duracao;
            }
        });
        med = med / cont;

        return {
            maximo: max,
            medio: med,
            minimo: min
        };
    }

}