import express, { Router } from 'express';
import bodyParser from 'body-parser';
import errorhandler from 'errorhandler';
import cors from 'cors';
import { routerPesoa } from './api/controladores/pessoa/atendimentoRota';
import { routerUnidade } from './api/controladores/unidade/unidadeRota';
const app = express();

app.set('port', 8080);
app.use(cors());
app.use(bodyParser.json());
app.use(routerPesoa);
app.use(routerUnidade);

export default app;