export interface UnidadeSaude {
    email: string,
    idUnidade: string,
    nome: string,
    endereco: string,
    capacidade: number
}