import { UnidadeSaude } from "./unidadeSaude";

export interface Atendimento {
    idAtendimento: string,
    date: Date;
    nome: string,
    endereço : {
        rua: string,
        bairro: string
    },
    laudo: boolean,
    test1?: boolean, 
    test2?: boolean,
    duracao: number,
    unidadeSaude: UnidadeSaude
}