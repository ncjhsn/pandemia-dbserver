import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  ChartErrorEvent,
  ChartMouseLeaveEvent,
  ChartMouseOverEvent,
  ChartSelectionChangedEvent,
  ChartType,
  Column,
  GoogleChartComponent
} from 'angular-google-charts';
import { ServicesService } from '../services.service';
import { Atendimento } from '../atendimento';

@Component({
  selector: 'app-estatisticas',
  templateUrl: './estatisticas.component.html',
  styleUrls: ['./estatisticas.component.scss']
})
export class EstatisticasComponent implements OnInit {
  data: any[] = [];
  public chartsTotal: {
    title: string;
    type: ChartType;
    data: any[][];
    columns?: Column[];
    options?: {};
  };
  public chartsPorDia: {
    title: string;
    type: ChartType;
    data: any[][];
    columns?: Column[];
    options?: {};
  };
  public chartsPorBairro: {
    title: string;
    type: ChartType;
    data: any[][];
    columns?: Column[];
    options?: {};
  };


  casosTotaisPorDia: {
    dia: string,
    casos: number
  }[] = [];
  casosNovosPorDia: {
    dia: string,
    casos: number
  }[] = [];
  casosPorBairro: {
    bairro: string,
    casos: number
  }[] = [];

  @ViewChild('chart', { static: false })
  public chart: GoogleChartComponent;

  constructor(private router: Router, private api: ServicesService) {
    this.api.getCasosTotaisPorDia().subscribe(data => {
      this.casosTotaisPorDia = data;
      let arrayCaseTotalByDataParaOGrafico = [];
      this.casosTotaisPorDia.forEach(casos => { arrayCaseTotalByDataParaOGrafico.push([casos.dia, casos.casos]) });
      this.chartsTotal = {
        title: 'Casos totais',
        type: ChartType.LineChart,
        columns: ['Dia', 'Casos'],
        data: arrayCaseTotalByDataParaOGrafico,
        options: {
          width: 800,
          height: 450,
          hAxis: {
            title: 'Data',
          },
          vAxis: {
            title: 'Casos',
          },
          curveType: 'function',
          colors: ['red', '#004411'],
        }
      };

    });
    this.api.getCasosNovosPorDia().subscribe(data => {
      this.casosNovosPorDia = data;
      let arrayCasosPorDiaParaOGrafico = [];
      this.casosNovosPorDia.forEach(casos => { arrayCasosPorDiaParaOGrafico.push([casos.dia, casos.casos]) });
      this.chartsPorDia = {
        title: 'Casos por dia',
        type: ChartType.ColumnChart,
        columns: ['Dia', 'Casos'],
        data: arrayCasosPorDiaParaOGrafico,
        options: {
          width: 800,
          height: 450,
          hAxis: {
            title: 'Data',
          },
          vAxis: {
            title: 'Casos',
          }
        }
      };
    });
    this.api.getCasosPorBairro().subscribe(data => {
      this.casosPorBairro = data;
      let arrayCasosPorEndereçoParaOGrafico = [];
      this.casosPorBairro.forEach(casos => arrayCasosPorEndereçoParaOGrafico.push([casos.bairro, casos.casos]));

      this.chartsPorBairro = {
        title: 'Casos por Bairro',
        type: ChartType.PieChart,
        data: arrayCasosPorEndereçoParaOGrafico,
        options: {
          width: 800,
          height: 450,
          chart: {
            title: 'Casos por bairro',
          },
          is3D: true,
        }
      };
    });


  }

  public onReady() {
    console.log('Chart ready');
  }

  public ngOnInit() {
    console.log(this.chart);
  }
}
