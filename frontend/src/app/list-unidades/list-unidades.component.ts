import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { UnidadeSaude } from '../unidadeSaude';
import { Atendimento } from '../atendimento';
import { MatDialog } from '@angular/material/dialog';
import { DetailsUnidadeComponent } from '../details-unidade/details-unidade.component';
export interface unidadeDialog {
  unidade: UnidadeSaude
}


@Component({
  selector: 'app-list-unidades',
  templateUrl: './list-unidades.component.html',
  styleUrls: ['./list-unidades.component.scss']
})
export class ListUnidadesComponent implements OnInit {

  unidades: Array<UnidadeSaude> = [];
  AtendimentosUnidade: Array<Atendimento> = [];
  unidadeId: string;
  unidadeNome;
  unidadeCapacidade;
  unidadeEndereco;

  constructor(private api: ServicesService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getUnidades();
  }
  openDialog(u: UnidadeSaude): void {

    const dialogRef = this.dialog.open(DetailsUnidadeComponent,
      {
        width: '700px',
        height: '500px',
        data: {
          unidade: u
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    })
  }


  getUnidades() {
    this.api.getUnidadesDeSaude().subscribe(data => {
      console.log(data);

      this.unidades = data;
    });
  }


  getUnidade(id: string) {
    this.api.getAtendimentosByUnidade(id).subscribe(data => {
      this.AtendimentosUnidade = data;
    });
  }
}
