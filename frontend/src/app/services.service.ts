import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Atendimento } from './atendimento';
import { UnidadeSaude } from './unidadeSaude';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  urlRoot = 'http://localhost:8080';
  options = { headers: new HttpHeaders().set('Content-type', 'application/json') };

  constructor(private http: HttpClient) { }

  getAtendimentos(): Observable<Atendimento[]> {
    return this.http.get<Atendimento[]>(`${this.urlRoot}/Atendimento/buscaAtendimentos`).pipe(catchError(this.handleError));
  }

  getUnidadeById(id: string): Observable<UnidadeSaude> {
    return this.http.get<UnidadeSaude>(`${this.urlRoot}/unidade/buscaUnidade/${id}`).pipe(catchError(this.handleError));
  }
  getTempoUnidade(id: string): Observable<{
    maximo: number;
    medio: number;
    minimo: number;
  }> {
    return this.http.get<{
      maximo: number;
      medio: number;
      minimo: number;
    }>(`${this.urlRoot}/Atendimento/tempoUnidade/${id}`).pipe(catchError(this.handleError));
  }

  getAtendimentosPorUnidade(unidade: String): Observable<Atendimento[]> {
    return this.http.get<Atendimento[]>(this.urlRoot + '/Atendimento/unidade/' + '/${unidade}').pipe(catchError(this.handleError));
  }

  getUnidadesDeSaude(): Observable<UnidadeSaude[]> {
    return this.http.get<UnidadeSaude[]>(this.urlRoot + '/unidade/buscaUnidades').pipe(
      catchError(this.handleError)
    );
  }

  getAtendimentosByUnidade(idUnidade: String): Observable<Atendimento[]> {
    return this.http.get<Atendimento[]>(this.urlRoot + '/Atendimento/unidade/' + idUnidade).pipe(catchError(this.handleError));
  }

  postAtendimento(Atendimento: Atendimento): Observable<Atendimento> {
    return this.http.post<Atendimento>(this.urlRoot + '/Atendimento/cadastraatendimento', Atendimento, this.options).pipe(catchError(this.handleError));
  }

  postUnidadeDeSaude(unidade: UnidadeSaude): Observable<UnidadeSaude> {
    return this.http.post<UnidadeSaude>(`${this.urlRoot}/unidade/cadastraUnidade`, unidade, this.options).pipe(catchError(this.handleError));
  }
  getUnidadeDeSaudePorEmail(e: string): Observable<UnidadeSaude> {
    return this.http.get<UnidadeSaude>(`${this.urlRoot}/unidade/dashboard/valida/${e}`).pipe(catchError(this.handleError));
  };

  patchAtendimento(Atendimento: Atendimento): Observable<Atendimento> {
    return this.http.patch<Atendimento>(this.urlRoot + '/Atendimento/alteraAtendimento', Atendimento, this.options).pipe(catchError(this.handleError));
  }
  getCasosTotaisPorDia(): Observable<{
    dia: string,
    casos: number
  }[]> {
    return this.http.get<{
      dia: string,
      casos: number
    }[]>(this.urlRoot + '/Atendimento/casosTotalPorDia').pipe(catchError(this.handleError));
  }
  getCasosNovosPorDia(): Observable<{
    dia: string,
    casos: number
  }[]> {
    return this.http.get<{
      dia: string,
      casos: number
    }[]>(this.urlRoot + '/Atendimento/casosNovosPorDia').pipe(catchError(this.handleError));

  }
  getCasosNovosPorUnidade(u: string): Observable<{
    dia: string,
    casos: number
  }[]> {
    return this.http.post<{
      dia: string,
      casos: number
    }[]>(this.urlRoot + '/Atendimento/getNovosPorDiaPorUnidade', { unidade: u }).pipe(catchError(this.handleError));

  }
  getCasosPorBairro(): Observable<{
    bairro: string,
    casos: number
  }[]> {
    return this.http.get<{
      bairro: string,
      casos: number
    }[]>(this.urlRoot + '/Atendimento/casosPorBairro').pipe(catchError(this.handleError));

  }
  postCadastro(client_id: string, email: string, password: string) {
    return this.http.post('https://dev-vjpz7bb3.us.auth0.com/dbconnections/signup', { client_id: client_id, conection: 'PandemiaDbServer', email: email, password: password }).pipe(catchError(this.handleError));
  }









  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

}
