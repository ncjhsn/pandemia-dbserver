import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../auth.service';
import { ServicesService } from '../services.service';
import { UnidadeSaude } from '../unidadeSaude';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Atendimento } from '../atendimento';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AddAtendimentoComponent } from '../add-atendimento/add-atendimento.component';
import { EditAtendimentoComponent } from '../edit-atendimento/edit-atendimento.component';

export interface DialogData {
  nome: string,
  data: Date,
  endereco: {
    rua: string,
    bairro: string
  },
  duracao: number,
  test1: boolean,
  test2: boolean

}
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  data: any;
  cadastrado: boolean;
  cadastro: FormGroup;
  cadastro2: FormGroup;
  unidade: UnidadeSaude;
  isLinear = true;
  dadosDaUnidade;
  addAtendimento: DialogData;
  editAtendimento: DialogData;
  columns = ['idAtendimento', 'nome', 'date', 'laudo', 'editar'];

  Atendimentonome: string;
  Atendimentodata: Date
  Atendimentorua: string;
  Atendimentobairro: string;
  Atendimentoduracao: number;
  Atendimentotest1: boolean = false;
  Atendimentotest2: boolean = false;

  porcentagem: number;

  aux: Atendimento;

  constructor(public Auth: AuthService, private api: ServicesService, private fb: FormBuilder, public dialog: MatDialog) {
    this.cadastro = this.fb.group({
      nome: ['', Validators.required],
    });
    this.cadastro2 = this.fb.group({
      endereço: ['', Validators.required],
      capacidade: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.Auth.userProfile$.subscribe(async data => {
      this.data = data;
      await this.getUnidade(data.email);
    });
  }

  dialogEdit(p): void {
    const dialogRef = this.dialog.open(EditAtendimentoComponent, {
      width: '500px',
      height: '500px',
      data: {
        nome: p.nome,
        data: p.date,
        endereco: {
          rua: p.endereço.rua,
          bairro: p.endereço.bairro
        },
        duracao: p.duracao,
        test1: p.test1,
        test2: p.test2
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.editAtendimento = result;
      this.aux = {
        idAtendimento: p.idAtendimento,
        nome: this.editAtendimento.nome,
        date: new Date(this.editAtendimento.data),
        endereço: {
          rua: this.editAtendimento.endereco.rua,
          bairro: this.editAtendimento.endereco.bairro
        },
        laudo: (p.test1 && p.test2)? true: false,
        test1: this.editAtendimento.test1,
        test2: this.editAtendimento.test2,
        duracao: this.editAtendimento.duracao,
        unidadeSaude: {
          idUnidade: this.unidade.idUnidade,
          nome: this.unidade.nome,
          email: this.unidade.email,
          endereco: this.unidade.endereco,
          capacidade: this.unidade.capacidade,
        }
      }
      this.api.patchAtendimento(this.aux).subscribe(data =>
        console.log("certo")
      );
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddAtendimentoComponent, {
      width: '500px',
      height: '500px',
      data: {
        nome: this.Atendimentonome,
        data: this.Atendimentodata,
        endereco: {
          rua: this.Atendimentorua,
          bairro: this.Atendimentobairro
        },
        duracao: this.Atendimentoduracao,
        test1: this.Atendimentotest1,
        test2: this.Atendimentotest2
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.addAtendimento = result;
      this.aux = {
        idAtendimento: '0',
        nome: this.addAtendimento.nome,
        date: new Date(this.addAtendimento.data),
        endereço: {
          rua: this.addAtendimento.endereco.rua,
          bairro: this.addAtendimento.endereco.bairro
        },
        laudo: false,
        test1: this.addAtendimento.test1,
        test2: this.addAtendimento.test2,
        duracao: this.addAtendimento.duracao,
        unidadeSaude: {
          idUnidade: this.unidade.idUnidade,
          nome: this.unidade.nome,
          email: this.unidade.email,
          endereco: this.unidade.endereco,
          capacidade: this.unidade.capacidade,
        }
      }
      this.api.postAtendimento(this.aux).subscribe(data =>
        console.log("certo")
      );
    });
  }

  async getUnidade(email: string) {
    this.api.getUnidadeDeSaudePorEmail(email).subscribe(async res => {
      this.unidade = res;
      if (this.unidade.email == this.data.email) {
        this.cadastrado = true;
        await this.getAtendimento();
      }
      else {
        this.cadastrado = false;
      }
    });
  }

  async getAtendimento(): Promise<Atendimento[] | void> {
    this.api.getAtendimentosByUnidade(this.unidade.idUnidade).subscribe((data) => {
      this.dadosDaUnidade = data;
      this.porcentagem = (data.length / this.unidade.capacidade) * 100;

    });
  }
  onSubmit(a1, a2) {
    console.log(this.data.email);
    console.log(a1);
    console.log(a2);
    this.api.postUnidadeDeSaude({
      idUnidade: '0',
      nome: a1.nome,
      email: this.data.email,
      endereco: a2.endereço,
      capacidade: a2.capacidade
    }).subscribe(result => console.log(result));
  }


}