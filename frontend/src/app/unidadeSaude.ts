export interface UnidadeSaude {
    idUnidade: string,
    nome: string,
    email: string,
    endereco: string,
    capacidade: number,
    quantidadeAtual?: number
}