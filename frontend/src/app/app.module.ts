import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetailsUnidadeComponent } from './details-unidade/details-unidade.component';
import { ListUnidadesComponent } from './list-unidades/list-unidades.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatBadgeModule } from '@angular/material/Badge';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientModule } from '@angular/common/http';
import { EstatisticasComponent } from './estatisticas/estatisticas.component';
import { NgxChartsModule } from '@swimlane/ngx-charts'
import { GoogleChartsModule } from 'angular-google-charts';
import { HomeComponent } from './home/home.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { AddAtendimentoComponent } from './add-atendimento/add-atendimento.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { EditAtendimentoComponent } from './edit-atendimento/edit-atendimento.component'



@NgModule({
  exports: [
    MatSidenavModule
  ],
  declarations: [
    AppComponent,
    DetailsUnidadeComponent,
    ListUnidadesComponent,
    LoginComponent,
    EstatisticasComponent,
    HomeComponent,
    DashboardComponent,
    AddAtendimentoComponent,
    EditAtendimentoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatGridListModule,
    MatTabsModule,
    MatBadgeModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    NgxChartsModule,
    GoogleChartsModule,
    MatStepperModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    MatCheckboxModule


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
