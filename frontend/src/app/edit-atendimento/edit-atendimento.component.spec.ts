import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAtendimentoComponent } from './edit-atendimento.component';

describe('EditAtendimentoComponent', () => {
  let component: EditAtendimentoComponent;
  let fixture: ComponentFixture<EditAtendimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAtendimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAtendimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
