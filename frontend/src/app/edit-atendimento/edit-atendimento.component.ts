import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../dashboard/dashboard.component';
import { ServicesService } from '../services.service';
import { Atendimento } from '../atendimento';

@Component({
  selector: 'app-edit-atendimento',
  templateUrl: './edit-atendimento.component.html',
  styleUrls: ['./edit-atendimento.component.scss']
})
export class EditAtendimentoComponent implements OnInit {
  constructor(public dialogEdit: MatDialogRef<EditAtendimentoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private api: ServicesService) { }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogEdit.close();
  }

}
