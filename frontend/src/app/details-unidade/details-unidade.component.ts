import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ServicesService } from '../services.service';
import { ActivatedRoute } from '@angular/router';
import { Atendimento } from '../atendimento';
import { UnidadeSaude } from '../unidadeSaude';
import { trigger, style, animate, transition, state } from '@angular/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { unidadeDialog } from '../list-unidades/list-unidades.component';
import {
  ChartErrorEvent,
  ChartMouseLeaveEvent,
  ChartMouseOverEvent,
  ChartSelectionChangedEvent,
  ChartType,
  Column,
  GoogleChartComponent
} from 'angular-google-charts';

@Component({
  selector: 'app-details-unidade',
  templateUrl: './details-unidade.component.html',
  styleUrls: ['./details-unidade.component.scss'],
  animations: [trigger('detailExpand', [
    state('collapsed', style({ height: '0px', minHeight: '0' })),
    state('expanded', style({ height: '*' })),
    transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
  ])]
})

export class DetailsUnidadeComponent implements OnInit {
  tempos: {
    maximo: number;
    medio: number;
    minimo: number;
  };
  atendimentos: Array<Atendimento> = [];
  unidade: UnidadeSaude;
  expandedElement: Atendimento | null;
  columns = ['idAtendimento', 'nome', 'date', 'laudo'];
  porcentagem: number;

  chartsPorDia: {
    title: string;
    type: ChartType;
    data: any[][];
    columns?: Column[];
    options?: {};
  };

  casosNovosPorDia: {
    dia: string,
    casos: number
  }[] = [];
  arrayCasosPorDiaParaOGrafico = [];

  constructor(
    private api: ServicesService,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<DetailsUnidadeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: unidadeDialog) { }

  async ngOnInit() {
    this.api.getTempoUnidade(this.data.unidade.idUnidade).subscribe(data => this.tempos=data);
    this.unidade = this.data.unidade;
    //this.getUnidade();
    await this.getAtendimentosUnidade();
    // console.log(this.atendimentos.length);
    this.api.getCasosNovosPorUnidade(this.unidade.idUnidade).subscribe(data => {
      this.casosNovosPorDia = data;
      let cont = 0;
      this.casosNovosPorDia.forEach(casos => {
        if (cont < 7) {
          this.arrayCasosPorDiaParaOGrafico.push([casos.dia, casos.casos])
        }
        cont++;
      });
      this.chartsPorDia = {
        title: 'Ultimos 7 dias de dados',
        type: ChartType.ColumnChart,
        columns: ['Dia', 'Casos'],
        data: this.arrayCasosPorDiaParaOGrafico,
        options: {
          width: 600,
          height: 500,
          hAxis: {
            title: 'Data',
          },
          vAxis: {
            title: 'Casos',
          }
        }
      };
    });
  }



  async getAtendimentosUnidade() {
    this.api.getAtendimentosByUnidade(this.unidade.idUnidade).subscribe(data => {
      this.atendimentos = data;
      this.porcentagem = ((this.atendimentos.length / this.unidade.capacidade) * 100);

    });
  }

  getUnidade() {
    this.api.getUnidadeById(this.route.snapshot.params['id']).subscribe(data => {
      this.unidade = data;
      console.log(this.unidade);
      console.log(this.porcentagem);
    });
  }


}
