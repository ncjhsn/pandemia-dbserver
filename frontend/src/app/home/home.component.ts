import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ServicesService } from '../services.service';
import { Atendimento } from '../atendimento';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  atendimentos: number = 0;
  casos: number = 0;

  constructor(private Auth: AuthService, private api: ServicesService) { }

  ngOnInit(): void {
    this.getCasos();
    this.getAtendimentos();
  }


  getCasos() {
    this.api.getAtendimentos().subscribe(data => {
      data.forEach(p => {
        if (p.laudo) {
          this.casos++;
        }
      });
    });
  }

  getAtendimentos() {
    this.api.getAtendimentos().subscribe(data => {
      data.forEach(p => {
        if (p) {
          this.atendimentos++;
        }
      });
    });
  }

}
