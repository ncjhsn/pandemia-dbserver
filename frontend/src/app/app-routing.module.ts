import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUnidadesComponent } from './list-unidades/list-unidades.component';
import { DetailsUnidadeComponent } from './details-unidade/details-unidade.component';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { EstatisticasComponent } from './estatisticas/estatisticas.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddAtendimentoComponent } from './add-atendimento/add-atendimento.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'unidades', component: ListUnidadesComponent },
  { path: 'unidades/unidade/:id', component: DetailsUnidadeComponent },
  { path: 'estatisticas', component: EstatisticasComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'addAtendimento', component: AddAtendimentoComponent },
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
