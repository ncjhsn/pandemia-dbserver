import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAtendimentoComponent } from './add-atendimento.component';

describe('AddAtendimentoComponent', () => {
  let component: AddAtendimentoComponent;
  let fixture: ComponentFixture<AddAtendimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAtendimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAtendimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
