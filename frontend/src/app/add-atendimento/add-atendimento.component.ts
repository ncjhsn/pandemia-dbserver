import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Atendimento } from '../atendimento';
import { DialogData } from '../dashboard/dashboard.component';

@Component({
  selector: 'app-add-atendimento',
  templateUrl: './add-atendimento.component.html',
  styleUrls: ['./add-atendimento.component.scss']
})
export class AddAtendimentoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddAtendimentoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

}
